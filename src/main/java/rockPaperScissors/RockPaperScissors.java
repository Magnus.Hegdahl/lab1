package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /* 	
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Random rng = new Random(0);
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> resultMessages = Arrays.asList("It's a tie!", "Human wins!", "Computer wins!");

    public void run() {
        do {

            int humanChoice = -1;

            while (humanChoice == -1) {
                String humanText = readInput(
                    "Let's play round " + roundCounter +
                    "\nYour choice (Rock/Paper/Scissors)?"
                );
                humanChoice = rpsChoices.indexOf(humanText.toLowerCase());
                if (humanChoice == -1)
                    System.out.printf("I do not understand %s. Could you try again?\n", humanText);
            }

            int computerChoice = rng.nextInt(3);

            // imagine the 3 options layed out on a circle.
            // then the result of the game can be determined from
            // the signed distance between the choise of the two players (mod 3)
            // 0 if tie, 1 if human won, 2 if computer won
            int result = (humanChoice+3-computerChoice)%3;

            if (result == 1) ++humanScore;
            if (result == 2) ++computerScore;

            System.out.printf(
                "Human chose %s, computer chose %s. %s\n",
                rpsChoices.get(humanChoice),
                rpsChoices.get(computerChoice),
                resultMessages.get(result)
            );

            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

        } while (readInput("Do you wish to continue playing? (y/n)?").toLowerCase().equals("y"));

        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
